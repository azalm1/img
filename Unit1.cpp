//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Unit1.h"
#include "math.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;

/* type definitions */
typedef unsigned char uint8;
typedef signed char sint8;
typedef unsigned short uint16;
typedef signed short sint16;
typedef unsigned int uint32;
typedef signed int sint32;
#define HYST_SIZE 256

#define DEBUG_DRAW

float fHystogram[HYST_SIZE] = {0};
uint8 u8Convert[HYST_SIZE] = {0};
  
void IMG_Equalize(float *pfHyst, uint32 u32PointsNumber);

//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------



void IMG_Equalize(float *pfHyst, uint32 u32PointsNumber)
{
  uint16 u16I = 0;
  float fTmp = 0;

  fTmp = ((float)HYST_SIZE)/((float)u32PointsNumber); /* calculate coefficient in advance */

  /* normalization */
  pfHyst[0] = fTmp*pfHyst[0];
  for(u16I = 1; u16I<HYST_SIZE; u16I++)
  {
      pfHyst[u16I] = fTmp*pfHyst[u16I];
      pfHyst[u16I] = (float)(pfHyst[u16I-1] + pfHyst[u16I]);
  }
#ifdef DEBUG_DRAW
  for(u16I = 1; u16I<HYST_SIZE; u16I++)
  {
      Form1->Series1->AddXY(u16I, (float)pfHyst[u16I]);
  }
#endif
}


void __fastcall TForm1::Button3Click(TObject *Sender)
{
  uint16 u16I = 0;
  uint16 u16J = 0;

  Image1->Picture->Bitmap->PixelFormat = pf8bit;
  Image1->Picture->LoadFromFile("C:\\Projects\\Img_apical\\input2.bmp");

    Series1->Clear();

    /* Clear array */
  for( u16I = 1; u16I<HYST_SIZE; u16I++ )
  {
       fHystogram[u16I] = 0;
  }


  /* Hystogram calculation */
  for( u16I = 0; u16I<Image1->Width; u16I++ )
  {
      for( u16J = 0; u16J<Image1->Height; u16J++ )
	 {
	   fHystogram[(uint8)(Image1->Canvas->Pixels[u16I][u16J])]++;
	 }
  }

  /*equalization*/
  IMG_Equalize( fHystogram, Image1->Width*Image1->Height );

  /* Write image to canvas */
  for(u16I = 0; u16I<Image1->Width; u16I++)
  {
	  for(u16J = 0; u16J<Image1->Height; u16J++)
	  {
		 Image1->Canvas->Pixels[u16I][u16J] =
                    RGB(fHystogram[(uint8)(Image1->Canvas->Pixels[u16I][u16J])],
                        fHystogram[(uint8)(Image1->Canvas->Pixels[u16I][u16J])],
                        fHystogram[(uint8)(Image1->Canvas->Pixels[u16I][u16J])]
                        );
	  }

  }
}
//---------------------------------------------------------------------------
